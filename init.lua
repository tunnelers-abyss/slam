if minetest.get_modpath("elements") then
	minetest.override_item("default:0", {
		groups = { bit = 1 }
	})

	minetest.override_item("default:1", {
		groups = { bit = 1 }
	})

	minetest.override_item("default:2", {
		groups = { not_in_creative_inventory = 1, bit = 1 }
	})
end

minetest.register_craftitem("slam:byte", {
        description = "Byte",
        inventory_image = "byte.png",
        stack_max = 1000000,
})

minetest.register_craft({
	output = "slam:byte",
	type = "shapeless",
	recipe = {"group:bit", "group:bit", "group:bit", "group:bit", "group:bit", "group:bit", "group:bit", "group:bit"},
})

minetest.register_craftitem("slam:float", {
        description = "Float",
        inventory_image = "float.png",
        stack_max = 1000000,
})

minetest.register_craft({
	output = "slam:float",
	recipe = {
		{"", "", "slam:byte"},
		{"slam:byte", "", "slam:byte"},
		{"", "", "slam:byte"}
	}
})

minetest.register_craftitem("slam:brief", {
        description = "BRIEF Descriptor",
        inventory_image = "brief.png",
        stack_max = 1000000,
})

minetest.register_craftitem("slam:surf", {
        description = "SURF Descriptor",
        inventory_image = "surf.png",
        stack_max = 1000000,
})

minetest.register_craftitem("slam:usurf", {
        description = "U-SURF Descriptor",
        inventory_image = "usurf.png",
        stack_max = 1000000,
})

minetest.register_craftitem("slam:sift", {
        description = "SIFT Descriptor",
        inventory_image = "sift.png",
        stack_max = 1000000,
})

minetest.register_craftitem("slam:rootsift", {
        description = "RootSIFT Descriptor",
        inventory_image = "rootsift.png",
        stack_max = 1000000,
})

minetest.register_craftitem("slam:usift", {
        description = "U-SIFT Descriptor",
        inventory_image = "usift.png",
        stack_max = 1000000,
})

technic.register_compactor_recipe({input = {"slam:float 32"}, output = "slam:brief"})

minetest.register_craft({
	output = "slam:surf",
	type = "shapeless",
	recipe = {"slam:brief", "slam:brief"},
})

minetest.register_craft({
	output = "slam:sift",
	type = "shapeless",
	recipe = {"slam:surf", "slam:surf"},
})

minetest.register_craft({
	output = "slam:sift",
	type = "shapeless",
	recipe = {"slam:brief", "slam:brief", "slam:brief", "slam:brief"},
})

minetest.register_craft({
	output = "slam:sift",
	type = "shapeless",
	recipe = {"slam:usift"},
})

minetest.register_craft({
	output = "slam:sift",
	type = "shapeless",
	recipe = {"slam:rootsift"},
})

minetest.register_craft({
	output = "slam:usurf 2",
	recipe = 
		{{"", "slam:surf"},
		{"slam:surf", ""}},
})

minetest.register_craft({
	output = "slam:surf",
	type = "shapeless",
	recipe = {"slam:usurf"},
})

minetest.register_craft({
	output = "slam:usift 2",
	recipe = 
		{{"", "slam:sift"},
		{"slam:sift", ""}},
})

minetest.register_craft({
	output = "slam:rootsift 2",
	recipe = 
		{{"slam:sift"},
		{"slam:sift"}},
})

local descs = {
	{"SIFT", "sift"},
	{"U-SIFT", "usift"},
	{"RootSIFT", "rootsift"},
	{"SURF", "surf"},
	{"U-SURF", "usurf"},
	{"BRIEF", "brief"}
}

for _, d in ipairs(descs) do
	local bow = "slam:bow_"..d[2]
	local vlad = "slam:vlad_"..d[2]
	local densevlad = "slam:densevlad_"..d[2]
	minetest.register_craftitem(bow, {
		description = "Bag of Words ("..d[1]..")",
		inventory_image = "bow_"..d[2]..".png",
		stack_max = 1000000,
	})
	minetest.register_craftitem(vlad, {
		description = "VLAD ("..d[1]..")",
		inventory_image = "vlad.png^"..d[2]..".png",
		stack_max = 1000000,
	})
	minetest.register_craftitem(densevlad, {
		description = "DenseVLAD ("..d[1]..")",
		inventory_image = "densevlad.png^"..d[2]..".png",
		stack_max = 1000000,
	})
	technic.register_alloy_recipe({input = {"inventorybags:large_pouch", "slam:"..d[2].." 1024"}, output = bow, time = 5})
	minetest.register_craft({
		output = vlad,
		type = "shapeless",
		recipe = {bow, bow, bow, bow, bow, bow, bow, bow},
	})
	technic.register_compressor_recipe({input = {vlad.." 2"}, output = densevlad, time = 5})
end

